// stdlib functionality
#include <iostream>
// ROOT functionality
#include <TFile.h>
#include <TH1F.h>
// ATLAS EDM functionality
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODJet/JetContainer.h"
#include "JetSelectionHelper/JetSelectionHelper.h"
// jet calibration
#include "AsgTools/AnaToolHandle.h"
#include "JetCalibTools/IJetCalibrationTool.h"

int main(int argc, char** argv) {

 // add jet selection helper
 JetSelectionHelper jet_selector;
 
 asg::AnaToolHandle<IJetCalibrationTool> JetCalibrationTool_handle;
 JetCalibrationTool_handle.setTypeAndName("JetCalibrationTool/MyCalibrationTool");
 
 //configure the tool using the latest recommended parameters
 JetCalibrationTool_handle.setProperty("JetCollection","AntiKt4EMTopo"                                                  );
 JetCalibrationTool_handle.setProperty("ConfigFile"   ,"JES_MC16Recommendation_Consolidated_EMTopo_Apr2019_Rel21.config");
 JetCalibrationTool_handle.setProperty("CalibSequence","JetArea_Residual_EtaJES_GSC_Smear"                              );
 JetCalibrationTool_handle.setProperty("CalibArea"    ,"00-04-82"                                                       );
 JetCalibrationTool_handle.setProperty("IsData"       ,false                                                            );
 
 JetCalibrationTool_handle.retrieve();
 
 //new histograms
 TH1F* jet_n    =new TH1F("jet number","",20,0,20);
 TH1F* mjj_raw      =new TH1F("mjj_raw","",20,0,500);
 TH1F* mjj_cut     =new TH1F("mjj_cut","",20,0,500);
 TH1F* mjj_cal      =new TH1F("mjj_cal","",20,0,500);
 TH1F* mjj_cut_cal     =new TH1F("mjj_cut_cal","",20,0,500);

 // initialize the xAOD EDM
 xAOD::Init();

 // open the input file
 TString inputFilePath = "/home/atlas/Bootcamp/Data/DAOD_EXOT27.17882744._000026.pool.root.1";

 if (argc >= 2)inputFilePath = argv[1];
 
 TString outputFilePath = "outputPlot.root";
 if (argc >= 3) outputFilePath = argv[2];
 
 
 xAOD::TEvent event;
 std::unique_ptr< TFile > iFile ( TFile::Open(inputFilePath, "READ") );
 if(!iFile) return 1;
 event.readFrom( iFile.get() );
 
 // get the number of events in the file to loop over
  Long64_t numEntries = event.getEntries();
  if (argc >= 4) numEntries = std::atoi(argv[3]);
  if (numEntries  == -1) numEntries = event.getEntries();
  
  // for counting events
  unsigned count = 0;

  // primary event loop
 for ( Long64_t i=0; i<numEntries; ++i ) {
    
    // Load the event
    event.getEntry( i );

    // Load xAOD::EventInfo and print the event info
    const xAOD::EventInfo * ei = nullptr;
    event.retrieve( ei, "EventInfo" );
    if (i%100==0) std::cout << "Processing run # " << ei->runNumber() << ", event # " << ei->eventNumber() << std::endl;

    // retrieve the jet container from the event store
    const xAOD::JetContainer* jets = nullptr;
    event.retrieve(jets, "AntiKt4EMTopoJets");

    std::vector<xAOD::Jet> jets_raw;
    std::vector<xAOD::Jet> jets_cal;
    std::vector<xAOD::Jet> jets_cut;
    std::vector<xAOD::Jet> jets_cut_cal;
    

    // loop through all of the jets and make selections with the helper
    for(const xAOD::Jet* jet : *jets) {
      
        jets_raw.push_back(*jet);
        
    	// calibrate the jet
		xAOD::Jet *calibratedjet;
		JetCalibrationTool_handle->calibratedCopy(*jet,calibratedjet);
		
		jets_cal.push_back(*calibratedjet);
      	
      	if (jet_selector.isJetGood(jet)) { //selection
        	jets_cut.push_back(*jet);
        }
        if (jet_selector.isJetGood(jet)) { 
        	jets_cut_cal.push_back(*calibratedjet);
        }
         
         // cleanup
		delete calibratedjet;
     } //end of jet loop
     
     jet_n->Fill(jets_raw.size());
     if (jets_raw.size() >=2) { mjj_raw->Fill(  (jets_raw.at(0).p4()+jets_raw.at(1).p4()).M()/1000.);}
     if (jets_cal.size() >=2) { mjj_cal->Fill(  (jets_cal.at(0).p4()+jets_cal.at(1).p4()).M()/1000.);}
     if (jets_cut.size() >=2) { mjj_cut->Fill(  (jets_cut.at(0).p4()+jets_cut.at(1).p4()).M()/1000.);}
     if (jets_cut_cal.size() >=2) { mjj_cut_cal->Fill(  (jets_cut_cal.at(0).p4()+jets_cut_cal.at(1).p4()).M()/1000.);}
      
    // counter for the number of events analyzed thus far
    	count += 1;
 } //end of the event loop

	TFile *f = new TFile (outputFilePath, "recreate");
	mjj_raw->Write();
	mjj_cut->Write();
	mjj_cal->Write();
	mjj_cut_cal->Write();
	jet_n->Write();
	
	f ->Close();
	
  // exit from the main function cleanly
  return 0;
}
